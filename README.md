
# Ciência de Dados

## Projeto: housing


## Ambiente Virtual

``` bash
# 0. Instalando o ambiente virtual

  # 0.1 virtualenv
  pip install virtualenv


# 1. Criando o ambiente virtual

  # 1.1 Utilizando o venv
  python3 -m venv venv

  # 1.2 Utilizando o virtualenv
  virtualenv -p python venv


# 2. Carregando o ambiente virtual

  # 2.1 No Linux (Debian) e venv
  source venv/bin/activate
  
  # 2.2 No Windows (11) e virtualenv
  .\venv\Scripts\activate 


# 3.0 Requisitos 
pip freeze

    # 3.1 Salvando requisitos
pip freeze > requirements.txt

    # 3.2 Instalando requisitos
pip install -r requirements.txt

# 4. Instalando kernel para Jupyter (se não tiver no requirements)
pip install ipykernel
pip install jupyter


# 5. Adicionando o ambiente virtual à lista de kernels
python3 -m ipykernel install --user --name=venv

    # 5.1 Excluindo o ambiente virtual da lista de kernels
jupyter-kernelspec uninstall venv

# 6. Executando Jupyter notebook
jupyter notebook
```
